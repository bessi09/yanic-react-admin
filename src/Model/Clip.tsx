import * as React from "react";
import {
    List,
    Datagrid,
    TextField,
    NumberField,
    ReferenceField,
    SimpleForm,
    ReferenceInput,
    SelectInput, TextInput, Create, useDataProvider, Edit
} from 'react-admin';
import {StartEnd, Video} from "./Util";
import {useEffect, useState} from "react";

export const ClipList = (props: any) => (
    <List {...props}>
        <Datagrid rowClick="edit">
            <TextField source="id"/>
            <TextField source="name"/>
            <ReferenceField source="videoId" reference="videos"><TextField source="id"/></ReferenceField>
            <NumberField source="start"/>
            <NumberField source="end"/>
        </Datagrid>
    </List>
);


export const ClipCreate = (props: any) => {

    const [url, setUrl] = useState("");

    const dataProvider = useDataProvider();

    const onInputChange = async (e: any) => {
        const result = await (await (dataProvider.getOne("videos", {id: e.target.value}))).data
        setUrl(result.url)
    }

    return <Create {...props}>
        <SimpleForm>
            <TextInput source={"name"}/>
            <ReferenceInput onChange={onInputChange} reference={"videos"} source={"videoId"}>
                <SelectInput optionText={"title"}/>
            </ReferenceInput>
            {url &&
            <Video url={url} mode={"CLIP"}/>
            }
        </SimpleForm>
    </Create>
}

export const ClipEdit = (props: any) => (
    <Edit {...props}>
        <SimpleForm>
            <TextInput source={"name"}/>
            <ReferenceInput reference={"videos"} source={"videoId"}>
                <SelectInput optionText={"title"}/>
            </ReferenceInput>
            <ClipVideoEdit/>
        </SimpleForm>
    </Edit>
)

const ClipVideoEdit = ({record}: any) => {
    const dataProvider = useDataProvider();
    const [url, setUrl] = useState("");

    useEffect(() => {
        dataProvider.getOne("videos", {id: record.videoId}).then(r => {
            setUrl(r?.data?.url)
        })
    })

    const startEnd: StartEnd = {
        start: record.start ? record.start : 0,
        end: record.end ? record.end : 0
    }

    return <Video url={url} mode={"CLIP"} startEnd={startEnd}/>
}


