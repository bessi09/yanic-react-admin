import * as React from "react";
import {
    List,
    Datagrid,
    TextField,
    SimpleForm,
    TextInput,
    Edit,
    DateField,
    DateInput,
    UrlField, ReferenceInput, SelectInput, Create,
} from 'react-admin';
import {Video} from "./Util";

export const VideoList = (props: any) => (
    <List {...props}>
        <Datagrid rowClick="edit">
            <TextField source="id"/>
            <TextField source="title"/>
            <UrlField source={"url"}/>
            <DateField source="date"/>
        </Datagrid>
    </List>
);

export const VideoEdit = (props: any) => (
    <Edit title={<VideoTitle/>} {...props}>
        <SimpleForm>
            <TextInput disabled source={"id"}/>
            <TextInput source="title"/>
            <DateInput source={"date"}/>
            <TextInput source={"url"}/>
            <VideoLocal/>
        </SimpleForm>
    </Edit>
)

export const VideoCreate = (props: any) => (
    <Create {...props}>
        <SimpleForm>
            <TextInput source={"title"}/>
            <TextInput source={"url"} />
            <DateInput source={"date"} />
        </SimpleForm>
    </Create>
)

const VideoLocal = ({record}: any) => {
    return <Video url={record.url}/>
}


const VideoTitle = ({record}: any) => {
    return <span>Post {record ? `"${record.title}"` : ''}</span>;
}