import * as React from "react";
import Button from "@material-ui/core/Button";
import {useEffect, useRef, useState} from "react";
import {TextInput} from "react-admin";
import { useFormState } from 'react-final-form';


export interface StartEnd {
    start: number,
    end: number
}

export const Video = (props: any) => {
    let height = 340;
    let width = 510;
    const video = useRef(null)
    return <div style={{width: `${width}px`}}>
        <video ref={video} id="vid" controls key={props.url} width={width} height={height}>
            <source src={props.url} type="video/mp4"/>
        </video>
        {props.mode === "CLIP" && <VideoEdit video={video} startEnd={props.startEnd}/>}
    </div>
}

const VideoEdit = (props: any) => {
    const [start, setStart] = useState(props.startEnd ? props.startEnd.start : 0);
    const [end, setEnd] = useState(props.startEnd ? props.startEnd.end : 0);

    let player: HTMLVideoElement;

    const {values} = useFormState()

    useEffect(() => {
        if (start !== 0) {
            setupPlayer()
            player.currentTime = start
        }
    })

    const onStartClick = () => {
        setupPlayer()
        player.play()
        setStart(player.currentTime)
        values.start = player.currentTime
    }

    const onEndClick = () => {
        setupPlayer()
        player.pause()
        setEnd(player.currentTime)
        values.end = player.currentTime
    }

    const reset = () => {
        setupPlayer()
        setStart(0)
        setEnd(0)
        player.ontimeupdate = () => {
        }
    }

    const setupPlayer = () => {
        player = player ? player : props.video.current
    }

    const loop = () => {
        setupPlayer()
        player.currentTime = start
        player.play()
        player.ontimeupdate = () => {
            if (player.currentTime < start) {
                player.currentTime = start;
            }
            if (player.currentTime >= end) {
                player.currentTime = start;
                player.play()
            }
        }
    }

    return <div style={{textAlign: "center"}}>
        <Button variant="contained" onClick={onStartClick}>START</Button>
        <Button variant="contained" onClick={onEndClick}>END</Button>
        {(start !== 0 || end !== 0) &&
        <Button variant={"contained"} onClick={reset}>RESET</Button>
        }
        {start !== 0 && end !== 0 &&
        <Button variant={"contained"} onClick={loop}>LOOP</Button>
        }
        <div>
            {start !== 0 && <TextInput disabled source={"start"} defaultValue={start}/>}
            {end !== 0 && <TextInput disabled source={"end"} defaultValue={end}/>}
        </div>
    </div>
}