import React from 'react';
import './App.css';
import {Admin, Resource} from "react-admin";
import jsonServerProvider from 'ra-data-json-server';
import {Dashboard} from "./Dashboard";
import authProvider from "../authProvider";
import {ClipCreate, ClipEdit, ClipList} from "../Model/Clip";
import {VideoCreate, VideoEdit, VideoList} from "../Model/Video";
import VideoLibraryIcon from '@material-ui/icons/VideoLibrary';
import SlowMotionVideoIcon from '@material-ui/icons/SlowMotionVideo';

const dataProvider = jsonServerProvider('http://localhost:3004');

class App extends React.Component {


    render() {
        return (
            <Admin dataProvider={dataProvider} dashboard={Dashboard} authProvider={authProvider}>
                <Resource name={"videos"} list={VideoList} icon={VideoLibraryIcon} create={VideoCreate} edit={VideoEdit}/>
                <Resource name={"clips"} list={ClipList} icon={SlowMotionVideoIcon} create={ClipCreate} edit={ClipEdit}/>
            </Admin>
        )
    }
}

export default App;